/**
 * @file
 */
(function ($) {
  "use strict";

  // Variables leftover from BETA site. Re-evaluate after search is
  // functional.
  // var headerOffset = 155;
  // var navOffset = 0;
  // var $nav = $('.nav');
  // var $leagueNav = $('.league-nav');
  // var $loginButton = $('.header__login > a[data-action="login"]');
  // var $mobileLoginButton = $('.mobile-nav__toplevel.login > a');
  // var $registerButton = $('.header__login > a[data-action="register"]');
  // var $searchWrapper = $('.searchbar-wrapper');
  // var $searchBar = $('.searchbar__field');

  // Mobile nav variables.
  var $body = $('body');
  var $header = $('#header');
  var $main = $('#main');
  var headerOffset = $('#header:visible').offset().top;
  var $window = $(window);
  var navLevel = 0;
  var $navWrapper = $('#block-mainnavigation');
  var $navWrapperMobile = $('#nav-mobile');
  var $navOverlay = $('#nav-mobile-overlay');
  var $hamburger = $('#nav-mobile-hamburger');
  var $activeSubNav = [];
  var $searchIcon = $('.searchbar__icon');
  var $searchBlock = $('.block-rwsearch');

  if ('ontouchstart' in document.documentElement) {
    // Replace hover with touch for touchscreen devices.
    $('.menu-item--expanded', $navWrapper).addClass('hover-disabled');
    $('.menu-item--expanded > a', $navWrapper).once('nav-subnav-touch').on('click touch', function(e) {
      e.preventDefault();
      var $menuItemExpanded = $('.menu-item--expanded');
      $(this).parent($menuItemExpanded).siblings().removeClass('active').find('.active').removeClass('active');
      $(this).closest($menuItemExpanded).toggleClass('active');
    })

    $(document).click(function(event) {
      if(!$(event.target).closest($navWrapper).length) {
        $('.menu-item--expanded.active').removeClass('active');
      }
    });
  }
  else {
    $('.js-menu-item-more', $navWrapper).once('nav-more').on('click', function(e) {
      e.preventDefault();
    })
  }

  function removeNavActives(hamburger) {
    $navWrapperMobile.css('height', '');
    $body.removeClass('no-scroll');
    $navOverlay.removeClass('active');
    hamburger.removeClass('nav__hamburger--prev');
    hamburger.removeClass('nav__hamburger--exit');
    $navWrapperMobile.removeClass('active');
    // Make sure the container is scrolled to the top.
    $navWrapperMobile.scrollTop(0);
  }

  function addNavActives(hamburger) {
    $navWrapperMobile.css('top', $header.position().top + $header.height() + 'px');
    $navWrapperMobile.css('height', 'calc(100vh - ' + $navWrapperMobile.offset().top + 'px)');
    if (!$body.hasClass('no-scroll')) {
      $body.addClass('no-scroll');
    }
    $navOverlay.addClass('active');
    hamburger.removeClass('nav__hamburger--prev');
    if (!hamburger.hasClass('nav__hamburger--exit')) {
      hamburger.addClass('nav__hamburger--exit');
    }
    if (!$navWrapperMobile.hasClass('active')) {
      $navWrapperMobile.addClass('active');
    }
  }
  // Reset mobile nav.
  function resetMobileNav() {
    $navWrapperMobile.css('height', '');
    navLevel = 0;
    // Make sure the container is scrolled to the top.
    $navWrapperMobile.scrollTop(0);
    $navWrapperMobile.removeClass(function (index, className) {
      return (className.match (/(^|\s)nav-level-\S+/g) || []).join(' ');
    });
    $navWrapperMobile.find('.menu')
      .removeClass('active')
      .removeClass('subnav-open');
    removeNavActives($hamburger);
  }

  function closeSubnav() {
    // Filter out the last active subnav.
    $activeSubNav = $activeSubNav.filter(function(item) {
      if (item.id === navLevel) {
        item.navItem.parent().closest('.menu').removeClass('subnav-open');
        item.navItem.removeClass('active');
        return;
      }
      return item;
    });
    $navWrapperMobile.removeClass('nav-level-' + navLevel);
    navLevel--;
    if (navLevel !== 0) {
      $navWrapperMobile.addClass('nav-level-' + navLevel);
    }

    resetMobileNav()
  }

  $(document).on('click', function(event) {
    if(!$(event.target).closest($navWrapperMobile).length && !$(event.target).is($hamburger)) {
      if ($hamburger.hasClass('nav__hamburger--exit')) {
        removeNavActives($hamburger);
        return;
      }
      if ($hamburger.hasClass('nav__hamburger--prev')) {
        closeSubnav();
        removeNavActives($hamburger);
      }
    }
  });

  $hamburger.once('nav-mobile-hamburger').on('click', function() {
    if ($(this).hasClass('nav__hamburger--exit')) {
      removeNavActives($(this));
      return;
    }
    if ($(this).hasClass('nav__hamburger--prev')) {
      closeSubnav();
    }
    if (navLevel === 0) {
      addNavActives($(this));
    }

    if ($searchBlock.length != 0 && $searchBlock.hasClass('active')) {
      $searchBlock.removeClass('active');
    }
  });

  $navWrapperMobile.find('.menu-item--expanded > a').once('nav-mobile-subnav').on('click', function(e) {
    e.preventDefault();
    var self = $(this);
    // Make sure the container is scrolled to the top everytime.
    $navWrapperMobile.scrollTop(0);
    self.closest('.menu')
      .addClass('subnav-open');
    var subNav = self.next('.menu');
    subNav.addClass('active');
    $hamburger.addClass('nav__hamburger--prev');
    $hamburger.removeClass('nav__hamburger--exit');
    $navWrapperMobile.removeClass('nav-level-' + navLevel);
    navLevel++;
    $navWrapperMobile.addClass('nav-level-' + navLevel);
    // Add reference to new active subnav list.
    $activeSubNav.push({id: navLevel, navItem: subNav});
  });

  // Handle mobile sticky header.
  // $window.scroll(function() {
  //   // Fixed class.
  //   var fixedClass = 'sticky';
  //   if ($window.width() > 1023) {
  //     $header.removeClass(fixedClass);
  //     $main.css('padding-top', '');
  //     return;
  //   }
  //   if ($window.scrollTop() >= headerOffset
  //     && !$header.hasClass(fixedClass)) {
  //     $main.css('padding-top', $header.height());
  //     $header.addClass(fixedClass);
  //   }
  //   else if ($window.scrollTop() < headerOffset
  //     && $header.hasClass(fixedClass)) {
  //     $header.removeClass(fixedClass);
  //     $main.css('padding-top', '');
  //   }
  // });

  // $(window).resize(function() {
  //   resetMobileNav();
  // });

  // $searchIcon.on('click touch', function () {
  //   resetMobileNav();
  // })

})(jQuery);
