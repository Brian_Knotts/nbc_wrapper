(function ($, mps) {
  'use strict';

  /**
   * Obtain the current responsive set(i.e: full, tablet) from mps object on window resize, based on current user window
   * viewport (window.innerWidth), and Load New/Responsive Ad Sizes.
   *
   * Detailed information about every responsive set thresholds is available within mps.pagevars.responsive object.
   * Currently it's as following: {"0":[0,0],"full":[1024,0],"tabletwide":[729,0],"phablet":[664,0]}
   *
   * mps._rset - holds the responsive set detected by mps object on mps object load.
   * mps.getResponsiveSet() - gets the responsive set(i.e: full, tablet) for current window state.
   */
  $(window).on("resize deviceorientation", debounce(function() {
    // Check if mps object is available.
    if(typeof window.mps !== 'undefined'
      && typeof window.mps.responsiveApply === 'function'
      && typeof window.mps.getResponsiveSet === 'function'
    ) {
      // get the responsive set(i.e: full, tablet) for current window state.
      var current_rset = window.mps.getResponsiveSet();
      // skip if the responsive set hasn't changed
      if(current_rset === window.mps._rset) {
        return false;
      }
      // Re-detect Breakpoint Set and Load New Ad Sizes.
      window.mps.responsiveApply();
    }
  }, 500));

})(jQuery, mps);
