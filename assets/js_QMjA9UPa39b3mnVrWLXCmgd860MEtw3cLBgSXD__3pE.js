/* global a2a*/
(function (Drupal) {
  'use strict';

  Drupal.behaviors.addToAny = {
    attach: function (context, settings) {
      // If not the full document (it's probably AJAX), and window.a2a exists
      if (context !== document && window.a2a) {
        a2a.init_all('page'); // Init all uninitiated AddToAny instances
      }
    }
  };

})(Drupal);
;
;
window._taboola = window._taboola || [];
_taboola.push({article:'auto'});
!function (e, f, u, i) {
  if (!document.getElementById(i)) {
    e.async = 1;
    e.src = u;
    e.id = i;
    f.parentNode.insertBefore(e, f);
  }
}(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  'https://cdn.taboola.com/libtrc/nbcsports-rotoworld/loader.js',
  'tb_loader_script');
if (window.performance && typeof window.performance.mark == 'function') {
  window.performance.mark('tbl_ic');
}
;
/**
 * @file
 */
(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.rotoworldMobileNav = {
    attach: function (context, settings) {
      // Variables leftover from BETA site. Re-evaluate after search is
      // functional.
      // var headerOffset = 155;
      // var navOffset = 0;
      // var $nav = $('.nav');
      // var $leagueNav = $('.league-nav');
      // var $loginButton = $('.header__login > a[data-action="login"]');
      // var $mobileLoginButton = $('.mobile-nav__toplevel.login > a');
      // var $registerButton = $('.header__login > a[data-action="register"]');
      // var $searchWrapper = $('.searchbar-wrapper');
      // var $searchBar = $('.searchbar__field');

      // Mobile nav variables.
      var $body = $('body', context);
      var $header = $('#header', context);
      var $main = $('#main', context);
      var headerOffset = $header.offset().top;
      var $window = $(window);
      var navLevel = 0;
      var $navWrapper = $('#nav-mobile', context);
      var $navOverlay = $('#nav-mobile-overlay', context);
      var $hamburger = $('#nav-mobile-hamburger', context);
      var $activeSubNav = [];

      function removeNavActives(hamburger) {
        $navWrapper.css('height', '');
        $body.removeClass('no-scroll');
        $navOverlay.removeClass('active');
        hamburger.removeClass('nav__hamburger--prev');
        hamburger.removeClass('nav__hamburger--exit');
        $navWrapper.removeClass('active');
        // Make sure the container is scrolled to the top.
        $navWrapper.scrollTop(0);
      }

      function addNavActives(hamburger) {
        $navWrapper.css('height', 'calc(100% - ' + $navWrapper.offset().top + 'px)');
        if (!$body.hasClass('no-scroll')) {
          $body.addClass('no-scroll');
        }
        $navOverlay.addClass('active');
        hamburger.removeClass('nav__hamburger--prev');
        if (!hamburger.hasClass('nav__hamburger--exit')) {
          hamburger.addClass('nav__hamburger--exit');
        }
        if (!$navWrapper.hasClass('active')) {
          $navWrapper.addClass('active');
        }
      }
      // Reset mobile nav.
      function resetMobileNav() {
        $navWrapper.css('height', '');
        navLevel = 0;
        // Make sure the container is scrolled to the top.
        $navWrapper.scrollTop(0);
        $navWrapper.removeClass(function (index, className) {
          return (className.match (/(^|\s)nav-level-\S+/g) || []).join(' ');
        });
        $navWrapper.find('.menu')
          .removeClass('active')
          .removeClass('subnav-open');
        removeNavActives($hamburger);
      }

      $hamburger.once('nav-mobile-hamburger').click(function() {
        // BETA site logic for search field. Needs work after search
        // is functional.
        // if ( $('.nav__search').hasClass('active') ) {
        //   $('.nav__search').removeClass('active');
        //   $('.searchbar__field').removeClass('active');
        //   $('.searchbar-wrapper').removeClass('active');
        // }

        if ($(this).hasClass('nav__hamburger--exit')) {
          removeNavActives($(this));
          return;
        }
        if ($(this).hasClass('nav__hamburger--prev')) {
          // Filter out the last active subnav.
          $activeSubNav = $activeSubNav.filter(function(item) {
            if (item.id === navLevel) {
              item.navItem.parent().closest('.menu').removeClass('subnav-open');
              item.navItem.removeClass('active');
              return;
            }
            return item;
          });
          $navWrapper.removeClass('nav-level-' + navLevel);
          navLevel--;
          if (navLevel !== 0) {
            $navWrapper.addClass('nav-level-' + navLevel);
          }
          // Make sure the container is scrolled to the top everytime.
          $navWrapper.scrollTop(0);
        }
        if (navLevel === 0) {
          addNavActives($(this));
        }
      });

      $navWrapper.find('.menu-item--expanded > a').once('nav-mobile-subnav').click(function(e) {
        e.preventDefault();
        var self = $(this);
        // Make sure the container is scrolled to the top everytime.
        $navWrapper.scrollTop(0);
        self.closest('.menu')
          .addClass('subnav-open');
        var subNav = self.next('.menu');
        subNav.addClass('active');
        $hamburger.addClass('nav__hamburger--prev');
        $hamburger.removeClass('nav__hamburger--exit');
        $navWrapper.removeClass('nav-level-' + navLevel);
        navLevel++;
        $navWrapper.addClass('nav-level-' + navLevel);
        // Add reference to new active subnav list.
        $activeSubNav.push({id: navLevel, navItem: subNav});
      });

      // Handle mobile sticky header.
      $window.scroll(function() {
        // Fixed class.
        var fixedClass = 'sticky';
        if ($window.width() > 1023) {
          $header.removeClass(fixedClass);
          $main.css('padding-top', '');
          return;
        }
        if ($window.scrollTop() >= headerOffset
          && !$header.hasClass(fixedClass)) {
          $main.css('padding-top', $header.height()+45);
          $header.addClass(fixedClass);
        }
        else if ($window.scrollTop() < headerOffset
          && $header.hasClass(fixedClass)) {
          $header.removeClass(fixedClass);
          $main.css('padding-top', '');
        }
      });

      $(window).resize(function() {
        resetMobileNav();
      });
    }
  };

})(jQuery, Drupal);
;
